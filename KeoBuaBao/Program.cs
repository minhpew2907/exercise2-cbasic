﻿const int KEO = 1;
const int BUA = 2;
const int BAO = 3;

Console.WriteLine("Chon (KEO/BUA/BAO) theo so duoi day: ");
Console.WriteLine("1. KEO.");
Console.WriteLine("2. BUA.");
Console.WriteLine("3. BAO.");
Console.WriteLine("--------------------------------------");

Console.WriteLine("Nguoi choi 1: ");
int player1 = int.Parse(Console.ReadLine());
Console.WriteLine("Nguoi choi 2: ");
int player2 = int.Parse(Console.ReadLine());

if (player1 == KEO && player2 == BAO)
{
    Console.WriteLine("Nguoi choi 1 thang");
}
else if (player1 == BUA && player2 == KEO)
{
    Console.WriteLine("Nguoi choi 1 thang");
}
else if (player1 == BAO && player2 == BUA)
{
    Console.WriteLine("Nguoi choi 1 thang");
}
else if (player2 == KEO && player1 == BAO)
{
    Console.WriteLine("Nguoi choi 2 thang");
}
else if (player2 == BUA && player1 == KEO)
{
    Console.WriteLine("Nguoi choi 2 thang");
}
else if (player2 == BAO && player1 == BUA)
{
    Console.WriteLine("Nguoi choi 2 thang");
}
else if((player1 == KEO && player2 == KEO) || 
        (player1 == BUA && player2 == BUA) || 
        (player1 == BAO && player2 == BAO))
{
    Console.WriteLine("Hoa nhau");
}
